# config

[![Build Status](https://api.travis-ci.org/nortonlifelock/config.svg?branch=master)](https://travis-ci.org/nortonlifelock/config)
[![Go Report Card](https://goreportcard.com/badge/github.com/nortonlifelock/config)](https://goreportcard.com/report/github.com/nortonlifelock/config)
[![GoDoc](https://godoc.org/github.com/nortonlifelock/config?status.svg)](https://godoc.org/github.com/nortonlifelock/config)
[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0) [![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg)](http://makeapullrequest.com)

config
