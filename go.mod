module github.com/nortonlifelock/config

go 1.13

require (
	github.com/nortonlifelock/crypto v1.0.1-0.20200127165412-c97765915ee8
	github.com/nortonlifelock/files v1.0.1-0.20200127165427-5178f1323f54
)
